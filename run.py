#!/usr/bin/env python
import sys

from app.interpreter import run
from app.memory import Memory
from app.output import Output
from app.parser import Parser

parser = Parser(sys.argv[1])
output = Output()
run(Memory(), parser, output)
print(output)
