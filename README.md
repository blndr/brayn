# Brayn
This is a Brainfuck interpreter... what a terrible yet funny language :-)
See here for a full description : https://en.wikipedia.org/wiki/Brainfuck

## Usage

You can `run.py` it with the provided `samples` :

```
./run.py samples/hello_world.bf
```

## Dependencies

* Python 3.x
* pytest
* pytest-cov

## Install requirements
```
. venv/bin/activate
pip install -r requirements.txt
```

## Unit test execution
```
. venv/bin/activate
pytest
```