"""
This module parses the source code, access the memory accordingly and writes the result to the output.
"""


def run(memory, parser, output):
    while not parser.end_of_file:
        run_instruction(memory, parser, output)
        parser.next_instruction()


def run_instruction(memory, parser, output):
    assembly[parser.current_instruction](memory, parser, output)


def _increment(memory, parser, output):
    memory.increment_at_pointer()


def _decrement(memory, parser, output):
    memory.decrement_at_pointer()


def _backward(memory, parser, output):
    memory.move_backward()


def _forward(memory, parser, output):
    memory.extend()
    memory.move_forward()


def _print(memory, parser, output):
    output.write(memory.read_at_pointer())


def _start_loop(memory, parser, output):
    if memory.read_at_pointer() == 0:
        parser.position = parser.find_matching_bracket()


def _end_loop(memory, parser, output):
    if memory.read_at_pointer() != 0:
        parser.position = parser.find_matching_bracket()


assembly = {
    '+': _increment,
    '-': _decrement,
    '>': _forward,
    '<': _backward,
    '.': _print,
    '[': _start_loop,
    ']': _end_loop
}
