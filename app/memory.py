class Memory:
    """
    Represents the Memory accessed by the Brainfuck interpreter.
    """

    def __init__(self):
        self.buffer = [0]
        self.pointer = 0

    def __str__(self):
        return str(self.buffer)

    def increment_at_pointer(self):
        self.buffer[self.pointer] += 1

    def decrement_at_pointer(self):
        self.buffer[self.pointer] -= 1

    def read_at_pointer(self):
        return self.buffer[self.pointer]

    def move_forward(self):
        self.pointer += 1

    def move_backward(self):
        self.pointer -= 1

    def extend(self):
        return self.buffer.append(0)
