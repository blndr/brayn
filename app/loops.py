"""
This module builds a dictionary of matching brackets positions in the source code.
"""

from enum import Enum

def map_loops(source_code):
    loops = []

    for index, instruction in enumerate(source_code):
        if instruction == '[':
            closing_index = _find_closing_bracket_position(source_code, index)
            loops.append((index, closing_index))
        elif instruction == ']':
            _check_for_opening_bracket(loops, index)
    return loops


def _find_closing_bracket_position(source_code, position):
    sub_routine = source_code[position + 1:]
    depth = 1
    for index, instruction in enumerate(sub_routine):
        depth += {'[': 1, ']': -1}.get(instruction, 0)
        if depth == 0:
            return position + index + 1
    raise ParsingError(position, ParsingError.looking_forward)


def _check_for_opening_bracket(loops, position):
    for loop in loops:
        if position in loop:
            return True
    raise ParsingError(position, ParsingError.looking_backward)

class ParsingError(Exception):
    looking_forward = 'No closing bracket for opening bracket at index %i'
    looking_backward = 'No opening bracket for closing bracket at index %i'

    def __init__(self, bracket_index, cause):
        self.message = cause % bracket_index

    def __str__(self):
        return self.message
