class Output:
    """
    Represents the stdout of the Brainfuck source code execution.
    """

    def __init__(self):
        self.buffer = []

    def __str__(self):
        return ''.join(self.buffer)

    def write(self, ascii):
        self.buffer.append(chr(ascii))
