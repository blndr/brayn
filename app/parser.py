from app.loops import map_loops


class Parser:
    """
    Represents the Parser reading the Brainfuck source code.
    """

    def __init__(self, source_file):
        with open(source_file, 'r') as file:
            self.source_code = file.read()

        self.position = 0
        self.program_length = len(self.source_code) - 1
        self.loops = map_loops(self.source_code)

    @property
    def end_of_file(self):
        return self.position > self.program_length

    @property
    def current_instruction(self):
        return self.source_code[self.position]

    def next_instruction(self):
        self.position += 1

    def find_matching_bracket(self):
        for loop in self.loops:
            if self.position in loop:
                return loop[0] if self.position == loop[1] else loop[1]
