from test.parser_test_double import TestableParser

class ParserTest:
    def test_find_closing_bracket_from_opening_bracket(self):
        # given
        parser = TestableParser([])
        parser.loops = [(1, 3), (4, 8)]
        parser.position = 1

        # when
        closing_position = parser.find_matching_bracket()

        # then
        assert closing_position == 3

    def test_find_opening_bracket_from_closing_bracket(self):
        # given
        parser = TestableParser([])
        parser.loops = [(1, 3), (4, 8)]
        parser.position = 8

        # when
        closing_position = parser.find_matching_bracket()

        # then
        assert closing_position == 4
