from app.loops import map_loops
from app.loops import ParsingError
from pytest import raises


class LoopsTest:
    def test_maps_a_single_loop(self):
        # given
        source_code = ['+', '[', '.', ']', '+']

        # when
        result = map_loops(source_code)

        # then
        assert result == [(1, 3)]

    def test_maps_a_two_sequential_loops(self):
        # given
        source_code = ['+', '[', '.', ']', '+', '[', '+', ']']

        # when
        result = map_loops(source_code)

        # then
        assert result == [(1, 3), (5, 7)]

    def test_maps_an_inner_loop(self):
        # given
        source_code = ['+', '[', '.', '[', '+', ']', '>', ']']

        # when
        result = map_loops(source_code)

        # then
        assert result == [(1, 7), (3, 5)]

    def test_raises_a_parsing_error_when_no_closing_bracket(self):
        # given
        source_code = ['+', '[', '.', '>']

        # when
        with raises(ParsingError) as e:
            result = map_loops(source_code)

        # then
        assert e.value.message == 'No closing bracket for opening bracket at index 1'

    def test_raises_a_parsing_error_when_no_opening_bracket(self):
        # given
        source_code = ['+', '.', '>', ']']

        # when
        with raises(ParsingError) as e:
            result = map_loops(source_code)

        # then
        assert e.value.message == 'No opening bracket for closing bracket at index 3'
