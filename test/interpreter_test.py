from app.interpreter import run_instruction
from app.memory import Memory
from app.output import Output
from app.parser import Parser
from test.parser_test_double import TestableParser


class InstructionTest:
    def setup_method(self, method):
        self.output = Output()
        self.memory = Memory()

    def test_increment(self):
        # given
        parser = TestableParser(['+'])
        parser.position = 0
        self.memory.pointer = 0

        # when
        run_instruction(self.memory, parser, self.output)

        # then
        assert self.memory.read_at_pointer() == 1

    def test_decrement(self):
        # given
        parser = TestableParser(['-'])
        parser.position = 0
        self.memory.pointer = 0

        # when
        run_instruction(self.memory, parser, self.output)

        # then
        assert self.memory.read_at_pointer() == -1

    def test_backward(self):
        # given
        parser = TestableParser(['+', '<'])
        parser.position = 1
        self.memory.pointer = 1

        # when
        run_instruction(self.memory, parser, self.output)

        # then
        assert self.memory.pointer == 0

    def test_forward(self):
        # given
        parser = TestableParser(['>', '+'])
        parser.position = 0
        self.memory.pointer = 0

        # when
        run_instruction(self.memory, parser, self.output)

        # then
        assert self.memory.pointer == 1
        assert self.memory.buffer == [0, 0]

    def test_print(self):
        # given
        parser = TestableParser(['+', '.'])
        parser.position = 1
        self.memory.pointer = 0
        self.memory.buffer = [66]
        self.output.buffer = ['A']

        # when
        run_instruction(self.memory, parser, self.output)

        # then
        assert str(self.output) == 'AB'

    def test_start_loop_when_memory_points_to_zero(self):
        # given
        parser = TestableParser(['+', '[', '>', '+', '+', ']', '>', '+'])
        parser.position = 1
        parser.loops = [(1, 5)]
        self.memory.pointer = 0
        self.memory.buffer = [0]

        # when
        run_instruction(self.memory, parser, self.output)

        # then
        assert parser.position == 5

    def test_start_loop_when_memory_points_to_non_zero(self):
        # given
        parser = TestableParser(['+', '[', '>', '+', '+', ']', '>', '+'])
        parser.position = 1
        self.memory.pointer = 0
        self.memory.buffer = [12]

        # when
        run_instruction(self.memory, parser, self.output)

        # then
        assert parser.position == 1

    def test_end_loop_when_memory_points_to_zero(self):
        # given
        parser = TestableParser(['+', '[', '>', '+', '+', ']', '>', '+'])
        parser.position = 5
        self.memory.pointer = 0
        self.memory.buffer = [0]

        # when
        run_instruction(self.memory, parser, self.output)

        # then
        assert parser.position == 5

    def test_end_loop_when_memory_points_to_non_zero(self):
        # given
        parser = TestableParser(['+', '[', '>', '+', '+', ']', '>', '+'])
        parser.position = 5
        parser.loops = [(1, 5)]
        self.memory.pointer = 0
        self.memory.buffer = [12]

        # when
        run_instruction(self.memory, parser, self.output)

        # then
        assert parser.position == 1
