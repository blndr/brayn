from app.interpreter import run
from app.memory import Memory
from app.output import Output
from app.parser import Parser


class AcceptanceTest:
    def test_hello_world(self):
        # given
        parser = Parser('samples/hello_world.bf')
        output = Output()

        # when
        run(Memory(), parser, output)

        # then
        assert str(output) == 'Hello World!\n'

    def test_fibonacci(self):
        # given
        parser = Parser('samples/fibonacci.bf')
        output = Output()

        # when
        run(Memory(), parser, output)

        # then
        assert str(output) == '1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89'
