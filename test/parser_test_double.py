from app.parser import Parser


class TestableParser(Parser):
    def __init__(self, source_code):
        self.source_code = source_code
        self.position = 0
        self.program_length = len(self.source_code) - 1
